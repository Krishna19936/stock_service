package com.example.stock.service;
import static org.junit.jupiter.api.Assertions.assertEquals;



import java.util.ArrayList;
import java.util.List;

 

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

 

import com.example.stock.entity.Stock;
import com.example.stock.repository.StockRepository;

@SpringBootTest
public class StockServiceTest {
	
	@Mock
    StockRepository stockRepository;
    
    @InjectMocks
    StockService stockService;

 

    @BeforeEach
    public void init() {

 

    }
    
    @Test
    public void getallstockTest() {
        List<Stock> list = new ArrayList<>();
        Stock s = new Stock(1, "bajaj", 56, "5", "increase", 50);
        list.add(s);
        Mockito.when(stockRepository.findAll()).thenReturn((list));
        assertEquals(list, stockService.fetchallstocks());
    }

}
