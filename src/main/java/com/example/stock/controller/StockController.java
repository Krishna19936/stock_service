package com.example.stock.controller;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

 

import com.example.stock.entity.Stock;
import com.example.stock.service.StockService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/")
@Api(value = "StockController")
public class StockController {
	
	@Autowired
    private StockService stockservice;

 

    @GetMapping("/stocks")
    @ApiOperation("fetch  AllStocks")
    public ResponseEntity<List<Stock>> getAllStocks() {

 

        return new ResponseEntity<>(stockservice.fetchallstocks(), HttpStatus.OK);

 

    }
    
    @PutMapping(value = "/{stockId}/{quantity}/{action}")
    @ApiOperation("update quantity")
    public void updateStockQuantity(@PathVariable("stockId") long stockId, @PathVariable("quantity") long quantity,  @PathVariable("action") String action) {
    	
    	

 
    	 stockservice.updatequantity(stockId,quantity,action);
       

 

    }
    
    @GetMapping("/stockvalidation/{stockId}")
    @ApiOperation("validate stock id")
    public Optional<Stock> validatestock(@PathVariable("stockId") long stockId) {
    	
    	return stockservice.checkstockexists(stockId);
    }
    
 
    
    

}
