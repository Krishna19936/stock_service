package com.example.stock.service;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

 

import com.example.stock.entity.Stock;
import com.example.stock.repository.StockRepository;

@Service
public class StockService {
	
	@Autowired
    private StockRepository stockrepository;

 

    public List<Stock> fetchallstocks() {
        
        return stockrepository.findAll();

}


	public void updatequantity(long stockId, long quantity, String action) {
		
		long existinfQuant = stockrepository.getstockquantity(stockId);
		if(action.equals("buy")) {
			
			long updatedquantity = existinfQuant - quantity; 
			stockrepository.updatelatestquantity(stockId, updatedquantity);
		}
		
        if(action.equals("sell")) {
			
			long updatedquantity = existinfQuant + quantity;
			stockrepository.updatelatestquantity(stockId, updatedquantity);
		}
        
		
		
		
	}
	
	public Optional<Stock> checkstockexists(long stockId) {
		
		return stockrepository.findById(stockId);
		
	}

}
