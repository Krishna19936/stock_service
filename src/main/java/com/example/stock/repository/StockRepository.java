package com.example.stock.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.stock.entity.Stock;

@Repository
public interface StockRepository extends JpaRepository<Stock,Long>{
	
	
	
	
	@Query(value="Select quantity from stock where stock_id = ?1",nativeQuery=true)
	public long getstockquantity(long stockId);
	
	
	@Transactional
	@Modifying
	@Query(value="Update stock set quantity = ?2 where stock_id = ?1",nativeQuery=true)
	public void updatelatestquantity(long stockId, long quantity);
	
	

}
