package com.example.stock.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "stock")
public class Stock {
	

		@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    @Column(name = "stockId")
	    private long stockId;
		
		@Column(name = "stockName")
	    private String stockName;
		
		@Column(name = "stockPrice")
	    private double stockPrice;
		
		@Column(name = "diffPrevDay")
	    private String diffPrevDay;
		
		@Column(name = "trend")
	    private String trend;
		
		@Column(name = "quantity")
	    private long quantity;

		public long getStockId() {
			return stockId;
		}

		public void setStockId(long stockId) {
			this.stockId = stockId;
		}

		

		public double getStockPrice() {
			return stockPrice;
		}

		public void setStockPrice(double stockPrice) {
			this.stockPrice = stockPrice;
		}

		public String getDiffPrevDay() {
			return diffPrevDay;
		}

		public void setDiffPrevDay(String diffPrevDay) {
			this.diffPrevDay = diffPrevDay;
		}

		public String getTrend() {
			return trend;
		}

		public void setTrend(String trend) {
			this.trend = trend;
		}

		public long getQuantity() {
			return quantity;
		}

		public void setQuantity(long quantity) {
			this.quantity = quantity;
		}

		public Stock(long stockId, String stockName, double stockPrice, String diffPrevDay, String trend, long quantity) {
			super();
			this.stockId = stockId;
			this.stockName = stockName;
			this.stockPrice = stockPrice;
			this.diffPrevDay = diffPrevDay;
			this.trend = trend;
			this.quantity = quantity;
		}

		public String getStockName() {
			return stockName;
		}

		public void setStockName(String stockName) {
			this.stockName = stockName;
		}

		public Stock() {
			super();
		
		}
		
		


}
